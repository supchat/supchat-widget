let isExpanded = false;

s = document.createElement('link');
s.href = "https://supchat.chat/widget/css/style.css";
s.type = "text/css";
s.rel = "stylesheet";
s.async = !0;
e = document.getElementsByTagName('head')[0];
e.appendChild(s);
s.addEventListener('load', function (e) {
}, !1);

var appId = window.supchatSettings.app_id;

document.body.insertAdjacentHTML('beforeend',
    `<div class="supchat_wrapper" id="supchat_wrapper">
<div class="supchat_fireframe_wrapper" id="supchat_iframe_wrapper">
        <div class="supchat_chat_window_wrapper_default" id="supchat_window">
            <iframe width="100%" height="100%" src="https://supchat-65619.firebaseapp.com/?app_id=${appId}" frameborder="0"
             class="supchat_iframe"></iframe>
        </div>
        
</div>
<div class="supchat_fab" id="supchat_fab">
            <div class="supchat_chat_fab_icon_default" id="supchat_fab_icon"></div>
    </div>
</div>
`);

document.getElementById("supchat_fab").addEventListener("click", () => {
    console.log("click clack");
    isExpanded = !isExpanded;

    update();
});

function update() {
    let win = document.getElementById("supchat_window");
    let btnIcon = document.getElementById("supchat_fab_icon");
    let wrapper = document.getElementById("supchat_wrapper");
    let iFrameWrapper = document.getElementById("supchat_iframe_wrapper");
    if (isExpanded) {
        win.className = "supchat_chat_window_wrapper_expanded";
        btnIcon.className = "supchat_fab_icon_expanded";
        wrapper.className = "supchat_wrapper";
        iFrameWrapper.className = "supchat_fireframe_wrapper";
    } else {
        win.className = "supchat_chat_window_wrapper_collapsed";
        btnIcon.className = "supchat_fab_icon_collapsed";
        wrapper.className = "supchat_wrapper_collapsed";
        iFrameWrapper.className = "supchat_fireframe_wrapper_collapsed";
    }
}
